<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharacterItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('character_item', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('count')->nullable()->default(0);
			$table->integer('status')->nullable()->default(0);
			$table->integer('character_id')->nullable();
			$table->integer('item_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('character_item');
	}

}
