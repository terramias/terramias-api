<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->enum('gender', ['m', 'w'])->nullable();
            $table->datetime('last_login')->nullable();
            $table->unsignedDecimal('money', 10, 0)->default(0);
            $table->unsignedInteger('pearls')->default(0);
            $table->unsignedInteger('points')->default(0);
            $table->unsignedInteger('creative_points')->default(0);
            $table->unsignedInteger('generation')->default(0);
            $table->unsignedTinyInteger('generation_days')->default(0);
            $table->unsignedInteger('bonus_fights')->default(0);
            $table->unsignedInteger('bonus_games')->default(0);
            $table->unsignedInteger('bonus_ducks')->default(0);
            $table->unsignedInteger('highscore_points')->default(0);
            $table->unsignedInteger('level')->default(0);
            $table->unsignedInteger('level_points')->default(0);
            $table->unsignedInteger('inventory_level')->default(0);
            $table->unsignedInteger('figure_id')->nullable();
            $table->unsignedInteger('avatar_id')->nullable();
            $table->enum('status', ['new', 'active', 'inactive'])->default('new');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
}
