<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharacterMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('character_message', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('sent_at')->nullable();
			$table->integer('sender_id')->nullable();
			$table->integer('receiver_id')->nullable();
			$table->integer('message_id')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('character_message');
	}

}
