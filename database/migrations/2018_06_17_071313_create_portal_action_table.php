<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePortalActionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('portal_action', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('portal_id')->nullable();
			$table->integer('action_id')->nullable();
			$table->string('actiondata', 5196)->nullable();
			$table->integer('sortable_rank')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('portal_action');
	}

}
