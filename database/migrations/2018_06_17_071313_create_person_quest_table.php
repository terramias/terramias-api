<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonQuestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('person_quest', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('quest_id')->default(0);
			$table->integer('quest_step')->default(0);
			$table->integer('person_id')->default(0);
			$table->integer('land_id')->nullable()->default(0);
			$table->integer('map_x')->nullable()->default(0);
			$table->integer('map_y')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('person_quest');
	}

}
