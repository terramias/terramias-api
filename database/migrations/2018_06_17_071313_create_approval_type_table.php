<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApprovalTypeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('approval_types', function(Blueprint $table)
		{
			$table->integer('id');
			$table->string('entity_table', 64)->nullable();
			$table->string('entity_model', 64)->nullable();
			$table->string('entity_field', 64)->nullable();
			$table->integer('role_id');
			$table->timestamps();
			$table->primary(['id','role_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('approval_types');
	}

}
