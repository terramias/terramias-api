<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMapTooltipTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('map_tooltips', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('land_id');
			$table->string('polygon', 512);
			$table->string('title', 64);
			$table->string('content', 512);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('map_tooltips');
	}

}
