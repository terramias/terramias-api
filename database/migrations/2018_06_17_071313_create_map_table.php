<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMapTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('maps', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('land_id');
			$table->string('base_image', 32);
			$table->string('layer_image', 32);
			$table->integer('x');
			$table->integer('y');
			$table->integer('width')->default(700);
			$table->integer('height')->default(420);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('maps');
	}

}
