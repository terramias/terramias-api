<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWayTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ways', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('length')->default(1);
			$table->integer('width')->default(10);
			$table->integer('start_id');
			$table->integer('end_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ways');
	}

}
