<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFigureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('figures', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 64)->nullable();
			$table->string('image');
			$table->string('sprite_image')->nullable();
			$table->integer('sprite_height')->nullable()->default(0);
			$table->integer('sprite_width')->nullable()->default(0);
			$table->string('sprite_data', 10000)->nullable();
			$table->string('class', 32);
			$table->boolean('gender')->default(1);
			$table->boolean('type')->default(1);
			$table->boolean('status')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('figures');
	}

}
