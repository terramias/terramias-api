<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharacterCharacterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('character_character', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('source_id')->nullable();
			$table->integer('target_id')->nullable();
			$table->enum('status', ['active', 'inactive'])->default('active');
			$table->enum('type', ['friends', 'ignore'])->default('friends');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('character_character');
	}

}
