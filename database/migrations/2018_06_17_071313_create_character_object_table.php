<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharacterObjectTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('character_object', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('character_id');
			$table->integer('object_id');
			$table->integer('land_id');
			$table->integer('map_x')->default(0);
			$table->integer('map_y')->default(0);
			$table->integer('map_z')->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('character_object');
	}

}
