<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePortalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('portals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('land_id')->default(0);
			$table->integer('map_x')->default(0);
			$table->integer('map_y')->default(0);
			$table->integer('width')->default(0);
			$table->integer('height')->default(0);
			$table->integer('figure_id')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('portals');
	}

}
