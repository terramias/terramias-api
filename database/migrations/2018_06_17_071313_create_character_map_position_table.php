<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharacterMapPositionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('character_map_positions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('character_id')->nullable();
			$table->integer('land_id')->nullable();
			$table->integer('x')->nullable();
			$table->integer('y')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('character_map_positions');
	}

}
