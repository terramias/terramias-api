<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharacterQuestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('character_quest', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('quest_id')->nullable();
			$table->integer('character_id')->nullable();
			$table->boolean('status')->default(0);
			$table->integer('step')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('character_quest');
	}

}
