<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePortalQuestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('portal_quest', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('quest_id')->default(0);
			$table->integer('quest_step')->default(0);
			$table->integer('portal_id')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('portal_quest');
	}

}
