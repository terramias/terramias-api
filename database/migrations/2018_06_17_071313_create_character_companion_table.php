<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharacterCompanionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('character_companion', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('character_id')->nullable();
			$table->integer('companion_id')->nullable();
			$table->integer('health')->nullable();
			$table->boolean('status')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('character_companion');
	}

}
