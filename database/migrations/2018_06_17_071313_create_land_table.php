<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lands', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 128);
			$table->integer('generation');
			$table->string('background', 8)->default('#000000');
			$table->string('default_coords', 20)->default('0:0');
			$table->boolean('type')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lands');
	}

}
