<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionDialogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('action_dialogs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('dialog_id')->nullable();
			$table->integer('action_id')->nullable();
			$table->string('actiondata', 5196)->nullable();
			$table->integer('sortable_rank')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('action_dialogs');
	}

}
