<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjectTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('objects', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 128)->nullable();
			$table->boolean('type')->default(0);
			$table->string('image', 128)->nullable();
			$table->integer('width');
			$table->integer('height');
			$table->integer('sprite_width')->nullable();
			$table->integer('sprite_height')->nullable();
			$table->boolean('access')->nullable()->default(0);
			$table->boolean('status')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('objects');
	}

}
