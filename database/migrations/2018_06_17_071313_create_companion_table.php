<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 128)->nullable();
			$table->integer('figure_id')->nullable()->default(0);
			$table->string('animation', 1024)->nullable()->default('0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companions');
	}

}
