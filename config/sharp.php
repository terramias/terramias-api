<?php

return [
	"auth" => [
	    "login_attribute" => "email",
	    "password_attribute" => "password",
	    "display_attribute" => "name",
	],
    "entities" => [
        "user" => [
            "list" => App\Sharp\UserSharpList::class,
            "form" => \App\Sharp\UserSharpForm::class,
            // "validator" => \App\Sharp\UserSharpValidator::class,
            // "policy" => \App\Sharp\Policies\UserPolicy::class
        ],
        "character" => [
            "list" => App\Sharp\CharacterSharpList::class,
            "form" => \App\Sharp\CharacterSharpForm::class,
        //     "validator" => \App\Sharp\CharacterSharpValidator::class,
        //     "policy" => \App\Sharp\Policies\CharacterPolicy::class
        ],
    ],
	"menu" => [
	    [
	        "label" => "Spieler",
	        "entities" => [
	            "user" => [
	                "label" => "Accounts",
	                "icon" => "fa-user"
	            ],
	            "character" => [
	                "label" => "Charaktere",
	                "icon" => "fa-user"
	            ]
	        ]
	    ]
	]
];