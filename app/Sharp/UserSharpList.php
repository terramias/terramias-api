<?php
namespace App\Sharp;

use App\Models\User;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Code16\Sharp\EntityList\EntityListQueryParams;

class UserSharpList extends SharpEntityList
{

    function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make("name")
                ->setLabel("Name")
        )->addDataContainer(
            EntityListDataContainer::make("email")
                ->setLabel("Email")
        )->addDataContainer(
            EntityListDataContainer::make("characters")
                ->setLabel("Charaktere")
        );
    }

    function buildListConfig()
    {
    }

    function buildListLayout()
    {
        $this->addColumn("name", 4)
            ->addColumn("email", 4)
            ->addColumn("characters", 4);
    }

    function getListData(EntityListQueryParams $params)
    {
        return $this->transform(User::all());
    }
}