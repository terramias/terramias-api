<?php
namespace App\Sharp;

use App\Models\Character;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Code16\Sharp\EntityList\EntityListQueryParams;

class CharacterSharpList extends SharpEntityList
{

    function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make("name")
                ->setLabel("Name")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("avatar")
                ->setLabel("Avatar")
        )->addDataContainer(
            EntityListDataContainer::make("land")
                ->setLabel("Land")
                ->setSortable()
        );
    }

    function buildListConfig()
    {
	    $this->setInstanceIdAttribute("id")
	        ->setSearchable()
	        ->setDefaultSort("name", "asc")
	        ->setPaginated();
    }

    function buildListLayout()
    {
        $this->addColumn("name", 4)
            ->addColumn("avatar", 4)
            ->addColumn("land", 4);
    }

    function getListData(EntityListQueryParams $params)
    {
        return $this->transform(Character::orderBy($params->sortedBy(), $params->sortedDir())->paginate(30));
    }
}