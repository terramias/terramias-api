<?php

namespace App\Sharp;

use App\Models\User;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;

class UserSharpForm extends SharpForm
{
    function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make("name")
                ->setLabel("Name")
        )->addField(
            SharpFormTextField::make(
                "ratings"
            )
                ->setLabel("Ratings")
        );
    }

    function buildFormLayout()
    {
        $this->addColumn(6, function(FormLayoutColumn $column) {
            $column->withSingleField("name")
                ->withFields("team|6", "ratings|6");
        });
    }

    function find($id): array
    {
        return $this->transform(User::findOrFail($id));
    }

    function update($id, array $data)
    {   
        $instance = $id ? User::findOrFail($id) : new User;

        return tap($instance, function($user) use($data) {
            $this->save($user, $data);
        })->id;
    }

    function delete($id)
    {
    }
}