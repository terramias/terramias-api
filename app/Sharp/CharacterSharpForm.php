<?php

namespace App\Sharp;

use App\Models\Character;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Form\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;

class CharacterSharpForm extends SharpForm
{
    function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make("name")
                ->setLabel("Name")
        )->addField(
            SharpFormTextField::make(
                "ratings"
            )
                ->setLabel("Ratings")
        );
    }

    function buildFormLayout()
    {
        $this->addColumn(6, function(FormLayoutColumn $column) {
            $column->withSingleField("name")
                ->withFields("team|6", "ratings|6");
        });
    }

    function find($id): array
    {
        return $this->transform(Character::findOrFail($id));
    }

    function update($id, array $data)
    {   
        $instance = $id ? Character::findOrFail($id) : new Character;

        return tap($instance, function($character) use($data) {
            $this->save($character, $data);
        })->id;
    }

    function delete($id)
    {
    }
}