<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Character;
use App\Http\Requests\CharacterRequest;

class CharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
		/**
		 * @var \App\Models\User
		 */
        
    	$user = \Auth::user();
    	return $user->characters()->get();
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CharacterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CharacterRequest $request)
    {
        \Auth::user()->characters()->create($request->all());

        $this->handleAvatarUpload($request, $character);

        return response()->json([
            'message' => 'Successfully created character!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		/**
		 * @var \App\Models\User
		 */
    	$user = \Auth::user();
    	return $user->characters()->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CharacterRequest $request, $id)
    {
		/**
		 * @var \App\Models\User
		*/
		$user = \Auth::user();
		$character = $user->characters()->findOrFail($id);

        $this->handleAvatarUpload($request, $character);
    }

    protected function handleAvatarUpload(CharacterRequest $request, Character $character)
    {
        if (!$request->hasFile('avatar') || !$request->file('avatar')->isValid()) {
            return;
        }

        $filenameWithExt = $request->file('avatar')->getClientOriginalName();
        $extension = $request->file('avatar')->getClientOriginalExtension();

        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);            
        $fileNameToStore = $filename.'_'.time().'.'.$extension;                       
        $path = $request->file('avatar')->path();

        $$character->addMedia($path)->usingName($fileNameToStore)->toMediaCollection('user-image-' . $character->owner()->id);
        $character->avatar = $fileNameToStore;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		/**
		 * @var \App\Models\User
		*/
		$user = \Auth::user();
		return $user->characters()->findOrFail($id)->delete();
    }
}
