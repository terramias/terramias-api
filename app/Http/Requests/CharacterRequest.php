<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CharacterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = \Auth::user();

        if (!$this->isMethod('post')) {
            return !empty($user->characters()->findOrFail($this->input('id')));
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];

        if ($this->isMethod('post')) {
            $rules = $this->createRules();
        }

        if ($this->isMethod('put')) {
            $rules = $this->updateRules();
        }

        return $rules;
    }

    private function createRules() 
    {
        return [
            'name' => 'required|string',
            'avatar' => 'image|nullable|max:1999|mimes:jpeg,bmp,png,jpg,gif',
            'user_id' => 'required|numeric|same:'.\Auth::user()->id
        ];
    }

    private function updateRules() 
    {
        return [
            'id' => 'required|exists:characters'
        ];
    }
}
