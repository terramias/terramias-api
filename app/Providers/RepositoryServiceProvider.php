<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Interfaces\CharacterRepository::class, \App\Repositories\CharacterRepositoryEloquent::class);
        $this->app->bind(\App\Interfaces\CharacterRepository::class, \App\Repositories\CharacterRepositoryEloquent::class);
        //:end-bindings:
    }
}
