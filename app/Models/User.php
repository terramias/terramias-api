<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\ModelStatus\HasStatuses;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Delatbabel\Elocrypt\Elocrypt;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, HasStatuses, SoftDeletes, Elocrypt;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token'
    ];

    protected $dates = ['deleted_at'];

       
    /**
     * The attributes that should be encrypted on save.
     *
     * @var array
     */
    protected $encrypts = [
        'name'
    ];

    public function characters() {
        return $this->belongsToMany('App\Models\Character')->withPivot('type');
    }
}
