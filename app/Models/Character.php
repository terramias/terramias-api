<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Spatie\ModelStatus\HasStatuses;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;


class Character extends Model implements HasMedia
{
    use Notifiable, HasApiTokens, HasStatuses, SoftDeletes, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'gender',
        'last_login',
        'money',
        'pearls',
        'points',
        'creative_points',
        'generation',
        'generation_days',
        'bonus_fights',
        'bonus_games',
        'bonus_ducks',
        'highscore_points',
        'level',
        'level_points',
        'inventory_level',
        'figure_id',
        'avatar_id',
        'status'
    ];

    public function ignore() 
    {
        return $this->belongsToMany('App\Models\Character')->wherePivot('type', 'ignore');
    }

    public function friends() 
    {
        return $this->belongsToMany('App\Models\Character')->wherePivot('type', 'friends');
    }

    public function figure() 
    {
        return $this->hasOne('App\Models\Figure');
    }

    public function owner()
    {
        return $this->belongsToMany('App\Models\User')->wherePivot('type', 'own')->first();
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('user-image-' . $this->owner()->id)
            ->acceptsFile(function (File $file) {
                return $file->mimeType === 'image/jpeg';
            })
            ->useDisk('avatars');
    }
}
