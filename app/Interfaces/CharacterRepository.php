<?php

namespace App\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CharacterRepository.
 *
 * @package namespace App\Interfaces;
 */
interface CharacterRepository extends RepositoryInterface
{
    //
}
