<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Interfaces\CharacterRepository;
use App\Entities\Character;
use App\Validators\CharacterValidator;

/**
 * Class CharacterRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CharacterRepositoryEloquent extends BaseRepository implements CharacterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Character::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CharacterValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
